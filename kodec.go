/**
 * common codec library for chat application
 */
package kodec

import (
	"errors"
	"fmt"

	"github.com/golang/protobuf/proto"
)

const (
	BAG_ID_MSG byte = iota //bag id in for message
	BAG_ID_CMD
	BAG_ID_ACK
	BAG_ID_FREE
)

func BuildAck(id string) *Ack {
	m := &Ack{
		Id: id,
	}
	return m
}

func BuildCmd(tp Cmd_Type, d []byte, tick int64) *Cmd {
	m := &Cmd{
		Tp: tp,
		D:  d,
		Ct: tick,
	}
	return m
}

func BuildErrMsg(code uint32, msg string) *ResponsePackage {
	header := &CommunicationFrame{
		MsgType: MsgType_JOIN_ROOM_RES,
	}
	result := &Result{
		Code: code,
		Msg:  msg,
	}
	resp := &ResponsePackage{
		HeadFrame:   header,
		ResultFrame: result,
	}
	return resp
}

//func BuildMeta(tp Meta_Type, txt string) *Meta {
//	m := &Meta{
//		Tp:  &tp,
//		Txt: proto.String(txt),
//	}
//	return m
//}

//func BuildMessage(from uint64, to string, data []byte, tp MsgType, tick int64) *Msg {
//	m := &Msg{
//		From: from,
//		To:   to,
//		Tp:   tp,
//		D:    data,
//		Ct:   tick,
//	}
//	return m
//}

//func SetMessageId(m *Msg, id string) {
//	m.Id = id
//}

func Boxing(m proto.Message) ([]byte, error) {
	b, err := proto.Marshal(m)
	if err != nil {
		return nil, err
	}

	var bagId byte
	switch m.(type) {
	case *RequestPackage:
		bagId = BAG_ID_MSG
	case *ResponsePackage:
		bagId = BAG_ID_MSG
	case *Cmd:
		bagId = BAG_ID_CMD
	case *Ack:
		bagId = BAG_ID_ACK
	case *Free:
		bagId = BAG_ID_FREE
	default:
		return nil, errors.New("unknown frame")
	}
	return append([]byte{bagId}, b...), nil
}

func Unboxing(data []byte) (interface{}, error) {
	if len(data) < 1 {
		return nil, errors.New("warning: bad frame")
	}
	bagId := data[0]
	data = data[1:]

	switch bagId {
	case BAG_ID_MSG:
		if len(data) < 1 {
			return nil, errors.New("warning: bad message frame")
		}
		upMsg := new(RequestPackage)
		if err := proto.Unmarshal(data, upMsg); err != nil {
			return nil, fmt.Errorf("parse msg err %v", err)
		}

		return upMsg, nil
	case BAG_ID_ACK:
		ack := new(Ack)
		if err := proto.Unmarshal(data, ack); err != nil {
			return nil, fmt.Errorf("parse ack err %v", err)
		}
		return ack, nil
	case BAG_ID_CMD:
		cmd := new(Cmd)
		if err := proto.Unmarshal(data, cmd); err != nil {
			return nil, fmt.Errorf("parse cmd err %v", err)
		}
		return cmd, nil
	case BAG_ID_FREE:
		free := new(Free)
		if err := proto.Unmarshal(data, free); err != nil {
			return nil, fmt.Errorf("parse free err %v", err)
		}
		return free, nil
	default:
		return nil, fmt.Errorf("warning: unknown bag id: %v", bagId)
	}
}

func DecodeLogicalFrame(data []byte) (*LogicalResponse, error) {
	resp := new(LogicalResponse)
	if err := proto.Unmarshal(data, resp); err != nil {
		return nil, fmt.Errorf("parse ResponsePackage err %v", err)
	}
	return resp, nil
}

func EncodePong(userSize int64) []byte {
	m := &Pong{
		UserSize: userSize,
	}
	b, _ := proto.Marshal(m)
	return b
}

func DecodePing(data []byte) *Ping {
	ping := new(Ping)
	proto.Unmarshal(data, ping)
	return ping
}
