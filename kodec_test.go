package kodec

import (
	"testing"
	"time"
)

func TestBoxing(t *testing.T) {
	tick := time.Now().UnixNano() / int64(time.Millisecond)

	acks := []*Ack{
		BuildAck("123"),
		BuildAck("abc"),
	}

	for _, ack := range acks {
		if ack.String() == "" {
			t.Error("no")
		}
	}

	msgs := []*Msg{
		BuildMessage(123, "abc", []byte("hello, world!"), MsgType_JOIN_ROOM, tick),
		BuildMessage(456, "abc", []byte("good bye!"), MsgType_JOIN_ROOM, tick),
	}

	for _, msg := range msgs {
		if msg.To != "abc" {
			t.Error("no")
		}
	}

	cmds := []*Cmd{
		BuildCmd(Cmd_PING, "hi", tick),
		BuildCmd(Cmd_SYNC, "do", tick),
	}

	for _, cmd := range cmds {
		if cmd.Ct != tick {
			t.Error("no")
		}
	}
}

func TestUnboxing(t *testing.T) {

}
